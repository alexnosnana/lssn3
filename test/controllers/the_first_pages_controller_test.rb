require 'test_helper'

class TheFirstPagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get home_path
    assert_select "a[href=?]", home_path
    
   # get the_first_pages_home_url
    assert_response :success
    #assert_select "title", "this is HOME | lesson 3"
  end

  test "should get help" do
    get the_first_pages_help_url
    assert_response :success
    #assert_select "title", "this is HELP-PAGE | lesson 3"
  end
  
   test "should get about" do
    get about_path
    #get the_first_pages_about_url
    #assert_select "a[href=?]", about_path
    assert_response :success
    #assert_select "title", "this is ABOUT-PAGE | lesson 3"
  end

end
