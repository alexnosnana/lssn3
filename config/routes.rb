Rails.application.routes.draw do
  get 'sessions/new'

  get 'signup' => 'users#new'

 # get 'the_first_pages/home'
  
  get 'home'=> 'the_first_pages#home'

  get 'the_first_pages/help'
  
  # get 'the_first_pages/about'
  get 'about'=> 'the_first_pages#about'

  get 'for_first_heroku/ps_home'
  
  # for sessions
  get     'login'=> 'sessions#new'
  post    'login'=> 'sessions#create'
  delete  'logout'=> 'sessions#destroy'
  
  
  root 'the_first_pages#home'
  
  resources :users

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
