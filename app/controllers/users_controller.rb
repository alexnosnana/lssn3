class UsersController < ApplicationController
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end
  
  def create 
    @user = User.new(user_params) 
    if @user.save 
      flash[:success] = "регистрация успешна - молодец, млин... многого добился :)"
      redirect_to @user
    # обработка сохранения 
    else 
    render 'new' 
    end 
  end 
  private
  def user_params
    params.require(:user).permit(
      :name, 
      :email, 
      :sex, 
      :password, 
      :password_confirmation)
  end

end
