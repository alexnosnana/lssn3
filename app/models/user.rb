class User < ApplicationRecord
  before_save { self.email = email.downcase }
  validates :name, presence: true, length: {maximum: 18}
  VALID_EMAIL_REGEX = /\A([a-z]|[1-9]){1,45}\.?([a-z]|[1-9]){1,45}@([a-z]|[1-9]){1,45}\.[a-z]{2,9}\z/i
  validates :email, presence: true, length: {maximum: 27}, 
        format: {with: VALID_EMAIL_REGEX},
        uniqueness: { case_sensitive: false }
  validates :sex, length: {maximum: 9}
  has_secure_password
  validates :password, length: { minimum: 5 }
end
